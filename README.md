# setup

Notes on Arch Linux system setup.

## Notes

- Make sure you run in UEFI mode, meaning the output of `efivar -l` is not empty.
- Run `pacman -Syy archlinux-keyring` if running in key problems when doing the `pacstrap` step.
- The setup has been tested although there might be problems. Double-check your UEFI settings and check if there is nothing suspicious.
- The names of the disks might change, e.g. instead of `sdX` you might have `nvme0`, etc. Please replace the `sda` used in this tutorial according to your hardware setup.

### Preparation

- `cryptsetup open --type plain -d /dev/urandom /dev/sda temp` - create a temporary encrypted container named `temp` on the `/dev/sda` partition to be encrypted.
- `dd if=/dev/zero of=/dev/mapper/temp bs=4M status=progress oflag=sync` - fill the container with zeros.
- `cryptsetup close temp` - close the temporary container.

### Partitioning

- `parted /dev/sda mklabel gpt` - create a new label.
- `parted /dev/sda mkpart efi fat32 1M 512M` - create the EFI partition.
- `parted /dev/sda set 1 esp on` - set the EFI flag.
- `parted /dev/sda mkpart os ext4 512M 100%` - create the system partition.

### Encryption

- `cryptsetup luksFormat /dev/sda2` - initialize the LUKS2 header on the system partition.
- `cryptsetup luksDump /dev/sda2` - check the LUKS2 header.
- `cryptsetup luksOpen /dev/sda2 luks` - open the encrypted system partition and map it to `/dev/mapper/luks`.

### Logical volume management

- `pvcreate /dev/mapper/luks` - create a new physical volume from the encrypted system partition.
- `vgcreate vg /dev/mapper/luks` - create a new volume group `vg`.
- `lvcreate --size 32G --name root vg` - create a new logical volume `root` inside the `vg`.
- `lvcreate --extents 100%FREE --name home vg` - create a new logical volume `home` inside the `vg`, taking up the remaining space.

### Filesystems

- `mkfs.ext4 /dev/mapper/vg-root` - make a filesystem for the `vg-root` logical volume.
- `mkfs.ext4 /dev/mapper/vg-home` - make a filesystem for the `vg-home` logical volume.
- `mkfs.fat -F32 /dev/sda1` - make a filesystem for the `boot` partition.
- `mount /dev/mapper/vg-root /mnt` - mount the `vg-root`.
- `mkdir /mnt/home` - create the `home` dir.
- `mount /dev/mapper/vg-home /mnt/home` - mount the `home` dir.
- `mkdir /mnt/boot` - create the `boot` dir.
- `mount /dev/sda1 /mnt/boot` - create the `boot` dir.

### Installation

- `vim /etc/pacman.d/mirrorlist` - find the best mirror from the mirrorlist.
- `pacstrap /mnt linux linux-firmware lvm2 base base-devel zsh git vim iwd` - an alternative approach to what is described in Arch wiki. Execute the `pacstrap` command and follow instructions after `arch-chroot`.
- `pacstrap /mnt intel-ucode` - install microcode for Intel based CPUs (`amd-ucode` for AMD based CPUs).
- `genfstab -U /mnt > /mnt/etc/fstab` - write the currently mounted filesystems to the new system.
- `arch-chroot /mnt` - chroot to the your new OS.

### Configuration

#### User

- `useradd -s /bin/zsh -g wheel -m martins` - create a new user `martins`.
- `passwd martins` - set the password for `martins`.
- `passwd root` - set the password for `root`.
- `visudo` - edit `sudo` user permissions.

#### Locale

- `vim /etc/locale.gen` - edit the locale file.
- `locale-gen` - generate the chosen locales.
- `localectl set-locale LANG=en_US.UTF-8` - set the locale.

#### Hostname

- `vim /etc/hostname` - edit the hostname.

#### Boot

- `bootctl install` - install the systemd-boot bootloader.
- `vim /boot/loader/loader.conf` - edit the bootloader conf.

````
default arch
editor 0
timeout 3
console-mode max
````

- `cp /usr/share/systemd/bootcl/arch.conf /boot/loader/entries/` - copy the default entry.
- `vim /boot/loader/entries/arch.conf` - edit the default entry. Using `blkid`, `UUID` must be the `UUID` (not `PARTUUID`) of `/dev/sda2` (not of `/dev/mapper/luks` or `/dev/mapper/vg-root`).

````
title   Arch Linux
linux   /vmlinuz-linux
initrd  /intel-ucode.img
initrd  /initramfs-linux.img
options root=/dev/mapper/vg-root rd.luks.name=UUID=luks rd.luks.options=timeout=0 rootflags=x-systemd.device-timeout=0 quiet loglevel=3 vga=current
````

- `vim /etc/mkinitcpio.conf` - edit the initial ramdisk environment script.

````
...
HOOKS=(base systemd autodetect modconf keyboard block sd-encrypt lvm2 filesystems fsck)
...
````

- `mkinitcpio -p linux` - regenerate the initial ramdisk environment.

#### Network

- There is no need to install `dhcpd` since `systemd-networkd` can handle DHCP as well.
- `ip l` - find the names of the interfaces.
- `vim /etc/systemd/network/25-wired-wireless.network` - add a configuration file with the respective interfaces. The filename `25-wired-wireless` is not that important, you can use a different one.

````
[Match]
Name=enp2s0f0 wlan0

[Network]
DHCP=yes
````

- Enable / start services after the booting into the new system (see `Post` section below).

#### Packages

- `pacman -S reflector` - install `reflector` for optimizing mirrors.
- `reflector -p http --save /etc/pacman.d/mirrorlist` - find the best mirror and update mirrorlist.
- `su martins` - switch to the newly created user for running trizen as non-root.
- Follow the `trizen` setup [here](https://gitlab.com/sitilge/lists) and install the `base` list. Install other lists as you see fit.

#### Finalization

- `exit` - exit the environment. You might repeat till you get back to the Live USB.
- `umount -R /mnt` - recursively unmount the mounted partitions.

#### Post

- Reboot into the new system.
- `systemctl enable systemd-networkd.service` - enable the networking service.
- `systemctl start systemd-networkd.service` - start the networking service.
- `systemctl enable systemd-resolved.service` - enable the resolver service for local applications.
- `systemctl start systemd-resolved.service` - start the resolver service for local applications.
- `systemctl enable iwd.service` - enable the wireless service.
- `systemctl start iwd.service` - start the wireless service.
- `systemctl enable earlyoom` - enable userspace OOM daemon.
- `systemctl start earlyoom` - start userspace OOM daemon.
- `ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf` - redirect to the local stub DNS resolver file.
- `timedatectl set-timezone Europe/Riga` - set the timezone.
- `timedatectl set-ntp true` - enable network time sync.

#### Extra

- [lists](https://gitlab.com/sitilge/lists) - Arch Linux package lists.
- [dotfiles](https://gitlab.com/sitilge/dotfiles) - Linux dotfiles.
- [network](https://gitlab.com/sitilge/network) - notes on safe network setup.
- [gpg](https://gitlab.com/sitilge/gpg) - GPG key management.
